<?php

namespace App\Services;

use App\Models\Message;
use Illuminate\Database\Eloquent\Model;

class MessageService
{

    public function save($messageDTO): Model
    {
        $message = Message::query()->create($messageDTO);
        return $message;
    }

}
