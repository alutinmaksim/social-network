<?php

namespace App\Services;

use App\Models\PostImage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PostImageService
{

    public function save($file): Model|Builder
    {
        $path = Storage::disk('public')->put('/images', $file['image']);
        $image = PostImage::query()->create([
            'path' => $path,
            'user_id' => auth()->id()
        ]);
        return $image;
    }

}
