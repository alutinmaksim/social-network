<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserService
{

    public function followersCheck(Collection $users, array $followingIds): void
    {
        foreach ($users as $user) {
            if (in_array($user->id, $followingIds)) {
                $user->is_followed = true;
            }
        }
    }

    public function runToggleFollowing(User $user): array
    {
        $res = auth()->user()
            ->followings()
            ->toggle($user->id);
        $data['is_followed'] = count($res['attached']) > 0;
        return $data;
    }


}
