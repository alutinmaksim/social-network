<?php

namespace App\Services;

use App\Repositories\AvatarRepository;
use App\Models\Avatar;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class AvatarService
{

    public function __construct(private readonly AvatarRepository $repository)
    {
    }

    private function removeOldAvatar($avatar): void
    {
        Storage::disk('public')->delete($avatar->path);
        $avatar->delete();
    }

    private function createPath($data): string
    {
        $path = Storage::disk('public')->put('/avatars', $data['image']);
        return $path;
    }

    public function save($data): Builder|Model
    {
        $avatar = $this->repository->get();
        if ($avatar) {
            $this->removeOldAvatar($avatar);
        }
        $path = $this->createPath($data);
        $avatar = Avatar::query()
            ->create([
                'path' => $path,
                'user_id' => auth()->id()
            ]);
        return $avatar;
    }


}
