<?php

namespace App\Services;

use App\Repositories\PostImageRepository;
use App\Models\Comment;
use App\Models\Post;
use App\Models\PostImage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class PostService
{

    public function __construct(private readonly PostImageRepository $repository)
    {
    }

    private function checkPostForUnreadComments($post): void
    {
        $unreadComment = $post->comments()->where('is_read', false)->first();
        if ($unreadComment) {
            $post->unread_comment = true;
        }
    }

    private function checkPostForLiked($post, $likedPostIds): void
    {
        if (in_array($post->id, $likedPostIds)) {
            $post->is_liked = true;
        }
    }

    private function processImage($post, $imageId): void
    {
        if (isset($imageId)) {
            $image = $this->repository->get($imageId);
            $image->update([
                'status' => true,
                'post_id' => $post->id
            ]);
        }
    }

    public function checkPostForLikesAndUnreadComments($posts, $likedPostIds): void
    {
        foreach ($posts as $post) {
            $this->checkPostForLiked($post, $likedPostIds);
            $this->checkPostForUnreadComments($post);
        }
    }

    public function savePost($postDTO): Model|Builder|JsonResponse
    {
        try {
            DB::beginTransaction();

            $imageId = $postDTO['image_id'];
            unset($postDTO['image_id']);

            $post = Post::query()->create($postDTO);
            $this->processImage($post, $imageId);
            PostImage::clearStorage();

            DB::commit();
            return $post;
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => $exception->getMessage()]);
        }
    }

    public function saveRepost($repostDTO): void
    {
        Post::query()->create($repostDTO);
    }

    public function runToggleLike($post): array
    {
        $res = auth()->user()
            ->likedPosts()
            ->toggle($post->id);
        $data['is_liked'] = count($res['attached']) > 0;
        $data['likes_count'] = $post->likedUsers()->count();
        return $data;
    }

    public function saveComment($commentDTO): Model|Builder
    {
        $comment = Comment::query()->create($commentDTO);
        return $comment;
    }

}
