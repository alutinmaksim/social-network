<?php

namespace App\Factories;

use App\DTO\RepostStoreDTO;
use App\Models\Post;

class RepostStoreDTOFactory
{

    public static function make(array $repost, Post $post): array
    {
        $repost['user_id'] = auth()->id();
        $repost['reposted_id'] = $post->id;
        $repostDTO = new RepostStoreDTO(...$repost);
        return (array) $repostDTO;
    }

}
