<?php

namespace App\Factories;

use App\DTO\MessageStoreDTO;
use App\Models\User;

class MessageStoreDTOFactory
{

    public static function make(array $message, User $user): array
    {
        $message['from_id'] = auth()->id();
        $message['to_id'] = $user->id;
        $messageDTO = new MessageStoreDTO(...$message);
        return (array) $messageDTO;
    }

}
