<?php

namespace App\Factories;

use App\DTO\PostStoreDTO;

class PostStoreDTOFactory
{

    public static function make(array $post): array
    {
        $post['user_id'] = auth()->id();
        $postDTO = new PostStoreDTO(...$post);
        return (array) $postDTO;
    }

}
