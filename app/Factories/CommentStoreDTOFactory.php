<?php

namespace App\Factories;

use App\DTO\CommentStoreDTO;
use App\Models\Post;

class CommentStoreDTOFactory
{

    public static function make(array $comment, Post $post): array
    {
        $comment['post_id'] = $post->id;
        $comment['user_id'] = auth()->id();
        $commentDTO = new CommentStoreDTO(...$comment);
        return (array) $commentDTO;
    }

}
