<?php

namespace App\DTO;

final class MessageStoreDTO
{

    public function __construct(
        public readonly int $from_id,
        public readonly int $to_id,
        public readonly string $body,
    )
    {
    }

}
