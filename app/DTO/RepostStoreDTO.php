<?php

namespace App\DTO;

final class RepostStoreDTO
{

    public function __construct(
        public readonly string $title,
        public readonly string $content,
        public readonly int $user_id,
        public readonly int $reposted_id,
    )
    {
    }

}
