<?php

namespace App\DTO;

final class PostStoreDTO
{

    public function __construct(
        public readonly string $title,
        public readonly string $content,
        public readonly int $user_id,
        public readonly string|null $image_id,
    )
    {
    }

}
