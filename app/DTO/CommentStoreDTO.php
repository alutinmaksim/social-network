<?php

namespace App\DTO;

final class CommentStoreDTO
{

    public function __construct(
        public readonly string $body,
        public readonly int $user_id,
        public readonly int $post_id,
        public readonly int|null $parent_id,
        public readonly bool $is_read = false
    )
    {
    }

}
