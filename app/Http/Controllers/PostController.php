<?php

namespace App\Http\Controllers;

use App\Factories\CommentStoreDTOFactory;
use App\Factories\PostStoreDTOFactory;
use App\Factories\RepostStoreDTOFactory;
use App\Repositories\CommentRepository;
use App\Services\PostService;
use App\Repositories\PostRepository;
use App\Http\Requests\Post\CommentRequest;
use App\Http\Requests\Post\RepostRequest;
use App\Http\Requests\Post\StoreRequest;
use App\Http\Resources\Comment\CommentResource;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PostController extends Controller
{

    public function __construct(
        private readonly PostRepository    $repository,
        private readonly CommentRepository $commentRepository,
        private readonly PostService       $service
    )
    {
    }

    public function index(): array
    {
        $posts = $this->repository->all();
        $likedPostIds = $this->repository->getLikedPostIds();
        $this->service->checkPostForLikesAndUnreadComments($posts, $likedPostIds);
        return PostResource::collection($posts)->resolve();
    }

    public function store(StoreRequest $request): PostResource
    {
        $postDTO = PostStoreDTOFactory::make($request->validated());
        $post = $this->service->savePost($postDTO);
        return PostResource::make($post);
    }

    public function repost(RepostRequest $request, Post $post): void
    {
        $repostDTO = RepostStoreDTOFactory::make($request->validated(), $post);
        $this->service->saveRepost($repostDTO);
    }


    public function toggleLike(Post $post): array
    {
        $data = $this->service->runToggleLike($post);
        return $data;
    }

    public function comment(CommentRequest $request, Post $post): CommentResource
    {
        $commentDTO = CommentStoreDTOFactory::make($request->validated(), $post);
        $comment = $this->service->saveComment($commentDTO);
        return CommentResource::make($comment);
    }

    public function commentList(Post $post): array
    {
        $comments = $this->commentRepository->all($post);
        return CommentResource::collection($comments)->resolve();
    }
}
