<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\AboutUserRequest;
use App\Http\Requests\User\StatRequest;
use App\Http\Resources\Post\PostResource;
use App\Http\Resources\User\UserResource;
use App\Models\LikedPost;
use App\Models\Post;
use App\Models\SubscriberFollowing;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends Controller
{

    public function __construct(
        private readonly UserRepository $repository,
        private readonly Userservice    $service,
    )
    {
    }

    public function index(): AnonymousResourceCollection
    {
        $users = $this->repository->all();
        $followingIds = $this->repository->getFollowingIds();
        $this->service->followersCheck($users, $followingIds);
        return UserResource::collection($users);
    }

    public function aboutUser(AboutUserRequest $request): array
    {
        $data = $request->validated();
        $userId = $data['user_id'] ?? auth()->id();
        $user = $this->repository->getInfoAboutUser($userId);
        return UserResource::make($user)->resolve();
    }

    public function post(User $user): AnonymousResourceCollection
    {
        $posts = $this->repository->getPostsUser($user);
        return PostResource::collection($posts);
    }

    public function toggleFollowing(User $user): array
    {
        $data = $this->service->runToggleFollowing($user);
        return $data;
    }

    public function followingPost(): AnonymousResourceCollection
    {
        $followerIds = $this->repository->getFollowerIds();
        $likedPostIds = $this->repository->getLikedPostIds();
        $unlikedPostsFollowers = $this->repository->getUnlikedPostsFollowers($followerIds, $likedPostIds);
        return PostResource::collection($unlikedPostsFollowers);
    }

    public function stat(StatRequest $request): JsonResponse
    {
        $data = $request->validated();
        $userId = $data['user_id'] ?? auth()->id();
        $result = $this->repository->getUserStatistics($userId);
        return response()->json(['data' => $result]);
    }

    public function interlocutor(User $user)
    {
        return $user->name;
    }

}
