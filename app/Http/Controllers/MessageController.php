<?php

namespace App\Http\Controllers;

use App\Events\SendMessageEvent;
use App\Factories\MessageStoreDTOFactory;
use App\Repositories\MessageRepository;
use App\Http\Requests\Message\StoreRequest;
use App\Http\Resources\Message\MessageResource;
use App\Http\Resources\User\UserResource;
use App\Services\MessageService;
use App\Models\User;

class MessageController extends Controller
{

    public function __construct(
        private readonly MessageRepository $repository,
        private readonly MessageService    $service
    )
    {
    }

    public function index(User $user): array
    {
        $messages = $this->repository->all($user);
        return MessageResource::collection($messages)->resolve();
    }

    public function store(User $user, StoreRequest $request): array
    {
        $messageDTO = MessageStoreDTOFactory::make($request->validated(), $user);
        $message = $this->service->save($messageDTO);
        broadcast(new SendMessageEvent($message))->toOthers();
        return MessageResource::make($message)->resolve();
    }

    public function chatListUsers(): array
    {
        $chatListUsers = $this->repository->getChatListUsers();
        return UserResource::collection($chatListUsers)->resolve();
    }
}
