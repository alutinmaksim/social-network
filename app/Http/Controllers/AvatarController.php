<?php

namespace App\Http\Controllers;

use App\Repositories\AvatarRepository;
use App\Http\Requests\Avatar\StoreRequest;
use App\Http\Resources\Avatar\AvatarResource;
use App\Services\AvatarService;
use App\Models\User;

class AvatarController extends Controller
{

    public function __construct(
        private readonly AvatarRepository $repository,
        private readonly AvatarService    $service
    )
    {
    }

    public function show(User $user): AvatarResource
    {
        $avatar = $this->repository->get($user);
        return AvatarResource::make($avatar);
    }

    public function store(StoreRequest $request): AvatarResource
    {
        $avatar = $this->service->save($request->validated());
        return AvatarResource::make($avatar);
    }
}
