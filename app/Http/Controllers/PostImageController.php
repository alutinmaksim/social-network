<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostImage\StoreRequest;
use App\Http\Resources\PostImage\PostImageResource;
use App\Models\PostImage;
use App\Services\PostImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostImageController extends Controller
{

    public function __construct(private readonly PostImageService $service)
    {
    }

    public function store(StoreRequest $request): PostImageResource
    {
        $image = $this->service->save($request->validated());
        return PostImageResource::make($image);
    }
}
