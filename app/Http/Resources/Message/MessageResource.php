<?php

namespace App\Http\Resources\Message;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{

    public function toArray(Request $request): array
    {
        return [
            'from_id' => $this->from_id,
            'to_id' => $this->to_id,
            'body' => $this->body,
            'time' => $this->created_at->diffForHumans(),
        ];
    }

}
