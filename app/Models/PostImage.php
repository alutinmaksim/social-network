<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PostImage extends Model
{
    protected $table = 'post_images';
    protected $guarded = false;

    public function getUrlAttribute(): string
    {
        return url('storage/' . $this->path);
    }

    public static function clearStorage(): void
    {
        $images = PostImage::query()->where('user_id', auth()->id())
            ->whereNull('post_id')->get();

        foreach ($images as $image) {
            Storage::disk('public')->delete($image->path);
            $image->delete();
        }
    }
}
