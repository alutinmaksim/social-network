<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    use HasFactory;

    protected $table = 'avatars';
    protected $guarded = false;

    protected function url(): Attribute
    {
        return Attribute::make(
            get: fn () => url('storage/' . $this->path)
        );
    }

}
