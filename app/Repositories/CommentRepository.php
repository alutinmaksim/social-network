<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;

class CommentRepository
{

    public function all($post): Collection
    {
        $post->comments()->update(['is_read' => true]);
        $comments = $post->comments()
            ->latest()
            ->get();
        return $comments;
    }

}
