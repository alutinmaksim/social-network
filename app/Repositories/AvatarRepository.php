<?php

namespace App\Repositories;

use App\Models\Avatar;
use App\Models\User;

class AvatarRepository
{

    public function get(User|bool $user = false): object|null
    {
        $id = $user ? $user->id : auth()->id();
        return Avatar::query()
            ->where('user_id', $id)
            ->first();

    }

}
