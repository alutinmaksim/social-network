<?php

namespace App\Repositories;

use App\Models\PostImage;
use Illuminate\Database\Eloquent\Model;

class PostImageRepository
{

    public function get($imageId): Model
    {
        return PostImage::query()->find($imageId);
    }

}
