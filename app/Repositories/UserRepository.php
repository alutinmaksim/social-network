<?php

namespace App\Repositories;

use App\Models\LikedPost;
use App\Models\Post;
use App\Models\SubscriberFollowing;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserRepository
{

    public function getLikedPostIds(): array
    {
        $likedPostIds = LikedPost::query()
            ->where('user_id', auth()->id())
            ->get('post_id')
            ->pluck('post_id')
            ->toArray();
        return $likedPostIds;
    }

    public function all(): Collection|array
    {
        $users = User::query()
            ->whereNot('id', auth()->id())
            ->get();
        return $users;
    }

    public function getFollowingIds(): array
    {
        $followingIds = SubscriberFollowing::query()
            ->where('subscriber_id', auth()->id())
            ->get('following_id')
            ->pluck('following_id')
            ->toArray();
        return $followingIds;
    }

    public function getInfoAboutUser(int $userId): Model|Builder
    {
        $user = User::query()
            ->where('id', $userId)
            ->select(['id', 'name', 'email'])
            ->firstOrFail();
        return $user;
    }

    public function getPostsUser(User $user): Collection
    {
        $posts = $user->posts()
            ->withCount('repostedByPosts')
            ->latest()
            ->get();
        $likedPostIds = $this->getLikedPostIds();
        foreach ($posts as $post) {
            if (in_array($post->id, $likedPostIds)) {
                $post->is_liked = true;
            }
        }
        return $posts;
    }

    public function getFollowerIds(): array
    {
        $followerIds = auth()->user()
            ->followings()
            ->latest()
            ->get()
            ->pluck('id')
            ->toArray();
        return $followerIds;
    }

    public function getUnlikedPostsFollowers(array $followerIds, array $likedPostIds): Collection
    {
        $unlikedPostsFollowers = Post::query()
            ->whereIn('user_id', $followerIds)
            ->whereNotIn('id', $likedPostIds)
            ->withCount('repostedByPosts')
            ->get();
        return $unlikedPostsFollowers;
    }

    public function getUserStatistics(int $userId): array
    {
        $statistics = [];
        $statistics['subscribers_count'] = SubscriberFollowing::query()
            ->where('following_id', $userId)
            ->count();
        $statistics['followings_count'] = SubscriberFollowing::query()
            ->where('subscriber_id', $userId)
            ->count();
        $postIds = Post::query()
            ->where('user_id', $userId)
            ->get('id')
            ->pluck('id')
            ->toArray();
        $statistics['likes_count'] = LikedPost::query()
            ->whereIn('post_id', $postIds)
            ->count();
        $statistics['posts_count'] = count($postIds);
        return $statistics;
    }

}
