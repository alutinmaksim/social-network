<?php

namespace App\Repositories;

use App\Models\LikedPost;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;

class PostRepository
{

    public function all(): Collection
    {
       $posts = Post::query()
            ->where('user_id', auth()->id())
            ->withCount('repostedByPosts')
            ->latest()
            ->get();
       return $posts;
    }

    public function getLikedPostIds(): array
    {
        $likedPostIds = LikedPost::query()
            ->where('user_id', auth()->id())
            ->get('post_id')
            ->pluck('post_id')
            ->toArray();
        return $likedPostIds;
    }

}
