<?php

namespace App\Repositories;

use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class MessageRepository
{

    private function getUserIdsFromChatList(): array
    {
        return Message::query()
            ->where('to_id', auth()->id())
            ->get('from_id')
            ->pluck('from_id')
            ->toArray();
    }

    public function all(User $user): Collection|array
    {
        $ownId = auth()->id();
        $messages = Message::query()
            ->where([
                ['to_id', $user->id], ['from_id', $ownId]
            ])
            ->orWhere([
                ['from_id', $user->id], ['to_id', $ownId]
            ])
            ->latest()
            ->get();
        return $messages;
    }

    public function getChatListUsers(): Collection
    {
        $usersIds = $this->getUserIdsFromChatList();
        $chatListUsers = User::query()
            ->whereIn('id', $usersIds)
            ->get();
        return $chatListUsers;
    }

}
