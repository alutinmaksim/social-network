<?php

use App\Http\Controllers\AvatarController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostImageController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index']);
        Route::post('/about_user', [UserController::class, 'aboutUser']);
        Route::get('/{user}/interlocutor', [UserController::class, 'interlocutor']);
        Route::post('/avatar', [AvatarController::class, 'store']);
        Route::get('/{user}/posts', [UserController::class, 'post']);
        Route::post('/{user}/toggle_following', [UserController::class, 'toggleFollowing']);
        Route::get('/{user}/messages', [MessageController::class, 'index']);
        Route::post('/{user}/messages', [MessageController::class, 'store']);
        Route::get('/following_posts', [UserController::class, 'followingPost']);
        Route::post('/stats', [UserController::class, 'stat']);
        Route::get('/chat_list_users', [MessageController::class, 'chatListUsers']);
    });

    Route::group(['prefix' => 'posts'], function () {
        Route::post('/', [PostController::class, 'store']);
        Route::get('/', [PostController::class, 'index']);
        Route::post('/post_images', [PostImageController::class, 'store']);
        Route::post('/{post}/toggle_like', [PostController::class, 'toggleLike']);
        Route::post('/{post}/repost', [PostController::class, 'repost']);
        Route::post('/{post}/comment', [PostController::class, 'comment']);
        Route::get('/{post}/comment', [PostController::class, 'commentList']);
    });
});
